<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/index1', function () {
//     return view('asasaassasasas/index1');
// });

Route::get('/', function () {
    return view('register');
});

Route::get('/register', 'LoginControl@getRegister');
Route::get('/register', 'LoginControl@postRegister')->name('register');
Route::get('/login', 'LoginControl@getLogin');
Route::get('/login', 'LoginControl@postLogin')->name('login');



// Route::get('/Template', function () {
//     return view('Template1/Template');
// });

// Route::get('/Template', function () {
//     return view('Baru/index');
// });

// Route::get('/', function (){
// 	return view('welcome');
// })

// Route::get('/welcome/{id}', function($num){
// 	return 'welcome id'.$num;
// });

// Route::get('/selamatdatang', function () {
//     return view('welcome');
// });


// Route::view('/','welcome');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
